# Mac Print Resolution Utility

A small utility written in Swift to test/debug printer resolution issues on MacOS.

1) Displays printer name and resolution information from the driver after Page Setup or Print dialogs
2) Generates resolution test charts for 200, 300, 360 dpi (and doublings thereof) printers
3) Open images
4) Page Setup
5) Print current image
6) Save current image as TIFF
7) Copy current image
8) Paste images from clipboard
9) Displays pixel size and resolution of current image
10) Displays print settings dictionary after Page Setup or Print dialogs
11) Generates solid white, and solid black, images for testing
12) Gray, RGB, CMYK color modes for generated images


## License
MIT License
