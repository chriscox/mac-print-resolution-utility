//
//  PrintResolutionUtilityApp.swift
//  PrintResolutionUtility
//
//  Created by Chris Cox on December 5, 2023.
//

/*

Notes on drivers
https://www.cups.org/doc/postscript-driver.html
https://www.cups.org/doc/raster-driver.html
https://developer.apple.com/documentation/driverkit/creating_a_driver_using_the_driverkit_sdk
https://apple.stackexchange.com/questions/60084/how-can-i-list-all-the-installed-printer-drivers-on-mac-os-10-6
//Macintosh HD/Library/Printers/PPDs/Contents/Resources/


FUTURE:
    Density chart?
    Color test pages?
        general ramps?
        ink alignment?
    Get other info from printers?
        quality mode    -- PMQualityMode    , cupsPrintQuality in settings
        color model     -- PMColorSpaceModel, kPMColorSpaceModelCount, ColorModel in settings
        color profile?      -- don't see an API or dictionary entry for the actual printer profile, kind of a hole in the Apple print API
        paper name and size  -- NSPrintInfo
        PostScript or not?      PMPrinterIsPostScriptCapable(_:) PMPrinterIsPostScriptPrinter(_:_:)
    
    PMSessionGetDataFromSession - has to be called with specific keys
                                    don't see any dictionary access
    
    What can we get from the print CG Context?
        just transforms, which so far are always 1:1
 */

import SwiftUI

/* -------------------------------------------------------------------------------- */

class AppDelegate: NSObject, NSApplicationDelegate {
    private var aboutBoxWindowController: NSWindowController?

    func showAboutPanel() {
        if aboutBoxWindowController == nil {
            let styleMask: NSWindow.StyleMask = [.closable, .titled]
            let window = NSWindow()
            window.styleMask = styleMask
            window.title = "About \(Bundle.main.appName)"
            window.contentView = NSHostingView(rootView: AboutView())
            aboutBoxWindowController = NSWindowController(window: window)
        }
        aboutBoxWindowController?.showWindow(aboutBoxWindowController?.window)
    }
}

/* -------------------------------------------------------------------------------- */

@main
struct PrintResolutionUtilityApp: App {
    var mPrintInfo : NSPrintInfo
    var mPrintLayout : NSPageLayout
    @StateObject var workImage = workingImage.shared
    @StateObject var clipboardHelp = clipboardHelper.shared
    @NSApplicationDelegateAdaptor(AppDelegate.self) var appDelegate

    init()  {
        mPrintInfo = NSPrintInfo()
        mPrintInfo.orientation = .portrait
        mPrintInfo.isHorizontallyCentered = true
        mPrintInfo.isVerticallyCentered = true
        mPrintLayout = NSPageLayout()
    }
    
    func updatePrinterData() {
        let myInfo = lastPrinterInfo.shared
        
        // in Swift, NSPrinter seems to be missing everything useful
        let printer = mPrintInfo.printer
        myInfo.name = printer.name
        let printModel = String ( printer.type.rawValue )
        myInfo.model = printModel
        
        // clear these, just in case we can't get the info later
        myInfo.resolutionsH.removeAll()
        myInfo.resolutionsV.removeAll()
        myInfo.resH = 0
        myInfo.resV = 0
        
        let session = PMPrintSession(mPrintInfo.pmPrintSession())
        var currentPrinterOptional : PMPrinter?
        var err: OSStatus = noErr
        
        err = PMSessionGetCurrentPrinter( session, &currentPrinterOptional)
        if (err != noErr) { return }            // we can't continue without a printer
        guard let currentPrinter = currentPrinterOptional else { return }

        var thisRes : PMResolution = PMResolution(hRes: 0, vRes: 0)
        let settings = PMPrintSettings(mPrintInfo.pmPrintSettings())
        err = PMPrinterGetOutputResolution( currentPrinter, settings, &thisRes)
        if (err == noErr) {
            myInfo.resH = thisRes.hRes
            myInfo.resV = thisRes.vRes
        }
        
        var resCount : UInt32 = 0
        err = PMPrinterGetPrinterResolutionCount( currentPrinter, &resCount )
        if (err != noErr) { return }

        for index in 0..<resCount {
            err = PMPrinterGetIndexedPrinterResolution( currentPrinter, index+1, &thisRes )
            if (err == noErr) {
                myInfo.resolutionsH.append(thisRes.hRes)
                myInfo.resolutionsV.append(thisRes.vRes)
            }
        }
        
        // copy settings and convert them to strings
        myInfo.setSettingsDictionary( dict: mPrintInfo.printSettings )
        
        // useless, but copy just in case
        myInfo.setPrintDescriptionDictionary( dict: printer.deviceDescription )
        
    }
    
    func PrintImage() {
        let session = PMPrintSession(mPrintInfo.pmPrintSession())
        let settings = PMPrintSettings(mPrintInfo.pmPrintSettings())
        let pageFormat = PMPageFormat(mPrintInfo.pmPageFormat())
        
        PMSessionBeginCGDocumentNoDialog(session, settings, pageFormat)
        PMSessionBeginPageNoDialog(session, pageFormat, nil)
        
        // this must happen after a call to BeginPage, which comes after BeginCGDOcument
        var unmanagedContext : Unmanaged<CGContext>?
        PMSessionGetCGGraphicsContext(session, &unmanagedContext )
        let printContext : CGContext = unmanagedContext!.takeUnretainedValue()

        // we really don't want point sampling when downsampling the image
        printContext.interpolationQuality = CGInterpolationQuality.high

#if false
        let transform = printContext.ctm
        let deviceSpaceTransform = printContext.userSpaceToDeviceSpaceTransform
// so far these are always 1:1, on every printer tested
#endif

        let mImage : NSImage = workingImage.shared.image
        let imageSize : NSSize = mImage.size
        let imagePPI = workingImage.shared.ppi
        let imageWidth : CGFloat = imageSize.width * (72 / imagePPI)
        let imageHeight : CGFloat = imageSize.height * (72 / imagePPI)

        // get paper rect so we can center the image on the page
        var paperRect : PMRect = PMRect(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        PMGetAdjustedPaperRect(pageFormat, &paperRect)
        let paperWidth = paperRect.right - paperRect.left
        let paperHeight = paperRect.bottom - paperRect.top
        var centerX : CGFloat = ( paperWidth - imageWidth ) / 2.0
        var centerY : CGFloat = ( paperHeight - imageHeight ) / 2.0
        let imageRect = CGRectMake(centerX, centerY, imageWidth, imageHeight)
        
        var renderRect : NSRect = NSMakeRect(0,0,imageSize.width, imageSize.height)
        let imageCG : CGImage? = mImage.cgImage( forProposedRect: &renderRect, context: nil, hints: nil )
        var printRect : NSRect = imageRect
        
        // scale down and adjust center if image is larger than print area (usually happens with image that don't have a ppi value)
        if (imageWidth > paperWidth || imageHeight > paperHeight) {
            let maxScale = max( imageWidth/paperWidth, imageHeight/paperHeight )
            let imageScaledWidth = imageWidth / maxScale
            let imageScaledHeight = imageHeight / maxScale
            centerX = CGFloat( paperWidth - imageScaledWidth ) / 2.0
            centerY = CGFloat( paperHeight - imageScaledHeight ) / 2.0
            let scaledRect : NSRect = NSMakeRect(centerX,centerY,imageScaledWidth, imageScaledHeight)
            printRect = scaledRect
        }
        
        printContext.draw( imageCG!, in: printRect )
        
        PMSessionEndPageNoDialog(session)
        PMSessionEndDocumentNoDialog(session)
    }

    func doPageSetup() {
        let result = mPrintLayout.runModal(with: mPrintInfo)
        if (result == Int(NSApplication.ModalResponse.OK.rawValue)) {         // NSOKButton now obfuscated to Int(NSApplication.ModalResponse.OK.rawValue)
            updatePrinterData()
        }
    }
    
    func doPrint() {
        let printPanel = NSPrintPanel()
        let result = printPanel.runModal( with: mPrintInfo )
        if (result == Int(NSApplication.ModalResponse.OK.rawValue)) {         // NSOKButton now obfuscated to Int(NSApplication.ModalResponse.OK.rawValue)
            updatePrinterData()
            if (workImage.isEmpty == false) {
                PrintImage()
            }
        }
    }

    func doOpen() {
        workImage.chooseFile()
    }
    
    func doSave() {
        workImage.saveFile()
    }
    
    func doCopy() {
        NSPasteboard.general.declareTypes( [.tiff], owner: nil)
        let pasteboard = NSPasteboard.general
        pasteboard.setData( workImage.image.tiffRepresentation, forType: .tiff)
    }
    
    func doPaste() {
        guard let imgData = NSPasteboard.general.data(forType: NSPasteboard.PasteboardType.tiff) else { return }
        guard let temp : NSImage = NSImage(data: imgData) else { return }
        workImage.updateFromPaste( image: temp )
    }

    var body: some Scene {
        WindowGroup {
           MainView()
        }.commands {

            // file menu
            CommandGroup(replacing: CommandGroupPlacement.newItem) {
                Button("Open...") {
                    doOpen()
                }.keyboardShortcut("o")
                
                Button("Save...") {
                    doSave()
                }.keyboardShortcut("s")
                .disabled(workImage.isEmpty)
                
                Divider()
                
                Button("Page Setup...") {
                    doPageSetup()
                }.keyboardShortcut("p", modifiers: [.command, .shift])
                
                Button("Print...") {
                    doPrint()
                }.keyboardShortcut("p")
                
                Divider()
            }
            
            CommandGroup(replacing: CommandGroupPlacement.appInfo) {
                Button("About \(Bundle.main.appName)...",
                        action: { appDelegate.showAboutPanel() })
            }
            
            // edit menu
            CommandGroup(replacing: CommandGroupPlacement.pasteboard) {     // includes Cut,Copy,Paste,Delete,SelectAll
                Button("Copy") {
                    doCopy()
                }.keyboardShortcut("c")
                .disabled(workImage.isEmpty)
                
                Button("Paste") {
                    doPaste()
                }.keyboardShortcut("v")
                .disabled( false == clipboardHelp.pasteBoardHasImage )
            }
            
        }   // end commands
        
    }   // end body
    
}   // end App
