//
//  About.swift
//  PrintResolutionUtility
//
//  Created by Chris Cox on December 8, 2023.
//  Mostly cobbled together from StackOverflow answers
//

import SwiftUI

/* -------------------------------------------------------------------------------- */

struct AboutView: View {

    var body: some View {
        VStack(spacing: 12) {
            Image(nsImage: NSImage(named: "AppIcon")!)
            
            Text("\(Bundle.main.appName)")
                .font(.system(size: 20, weight: .bold))
            
            Text("Version: \(Bundle.main.appVersionLong) (\(Bundle.main.appBuild)) ")
            
            Text("Copyright © 2023 Chris Cox\nMIT License")
                .multilineTextAlignment(.center)
        }
        .padding(15)
        .frame(minWidth: 320, minHeight: 300)
    }
}

/* -------------------------------------------------------------------------------- */

extension Bundle {
    public var appName: String { getInfo("CFBundleName")  }
    public var appBuild: String { getInfo("CFBundleVersion") }
    public var appVersionLong: String { getInfo("CFBundleShortVersionString") }
    //public var displayName: String {getInfo("CFBundleDisplayName")}
    //public var identifier: String {getInfo("CFBundleIdentifier")}
    
    fileprivate func getInfo(_ str: String) -> String { infoDictionary?[str] as? String ?? "⚠️" }
}

/* -------------------------------------------------------------------------------- */

#Preview {
    AboutView()
}
