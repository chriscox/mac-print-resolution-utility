//
//  ContentView.swift
//  PrintResolutionUtility
//
//  Created by Chris Cox on December 5, 2023.
//

import SwiftUI

/* -------------------------------------------------------------------------------- */

func stringifyDictionary( dict: NSDictionary ) -> [String: String] {
    let tempDict = dict
    var newDict : [String: String] = [:]
    
    for (key, value) in tempDict {
        let keyString = "\(key)"
        
        if value is String || value is Int || value is UInt
                    || value is Double || value is Float {
            let numString = "\(value)"
            newDict[keyString] = numString     // need to handle specific cases
        } else {
            // Need to handle other value types!!!!
            let numString = "\(value)"
            newDict[keyString] = numString     // need to handle specific cases
        }
    }
    
    return newDict
}

/* -------------------------------------------------------------------------------- */

func stringifyDictionary( dict: [NSDeviceDescriptionKey : Any] ) -> [String: String] {
    let tempDict = dict
    var newDict : [String: String] = [:]
    
    for (key, value) in tempDict {
        let keyString = "\(key.rawValue)"
        
        if value is String || value is Int || value is UInt
                    || value is Double || value is Float {
            let numString = "\(value)"
            newDict[keyString] = numString     // need to handle specific cases
        } else {
            // Need to handle other value types!!!!
            let numString = "\(value)"
            newDict[keyString] = numString     // need to handle specific cases
        }
    }
    
    return newDict
}

/* -------------------------------------------------------------------------------- */

/// this will own the values from the last printer accessed
final class lastPrinterInfo : ObservableObject {

    /// The public singleton of our printer data
    static let shared = lastPrinterInfo()

    @Published var name = ""
    @Published var model = ""
    @Published var resH : Double = 0
    @Published var resV : Double = 0
 //   @Published var resolutionList : [PMResolution] = []           // usually fails to compile, but not always, with no diagnostic message -- seems to be dependent on other source in file
    @Published var resolutionsH: Array<Double> = []
    @Published var resolutionsV: Array<Double> = []
    @Published var settingsDict : [String: String] = [:]
    @Published var printDescDict : [String: String] = [:]
    
    /// convert an NSDictionary into a dictionary of strings, store to settingsDict
    func setSettingsDictionary( dict: NSDictionary ) {
        settingsDict = stringifyDictionary( dict: dict )
    }
    
    /// convert a Printer Description Dictionary into a dictionary of strings, store to settingsDict
    func setPrintDescriptionDictionary( dict: [NSDeviceDescriptionKey : Any] ) {
        printDescDict = stringifyDictionary( dict: dict )
    }

}

/* -------------------------------------------------------------------------------- */

/// this will help manage clipboard data. Because Swift folks really haven't thought through clipboards, er, pasteboards.
final class clipboardHelper : ObservableObject {
    @Published var pasteBoardHasImage : Bool = false
    @Published var pasteBoardChangeCount : Int = -2

    /// The public singleton of our class
    static let shared = clipboardHelper()

    func checkPasteboard() {
        if pasteBoardChangeCount == NSPasteboard.general.changeCount { return }
        pasteBoardChangeCount = NSPasteboard.general.changeCount
        
        // There really should be a simple check for data availability instead of copying data unecessarily,
        // which can cause a delay due to a promise commit in another app
        guard let imgData = NSPasteboard.general.data(forType: NSPasteboard.PasteboardType.tiff) else
            {
                pasteBoardHasImage = false
                return
            }
        pasteBoardHasImage = true
    }
}

/* -------------------------------------------------------------------------------- */

struct MainView: View {
    let imageFrameInches = 4.0
//    let imageViewWidth = 72 * imageFrameInches    // can't use constants to init other constants at init time in Swift, lame!
    let columns = [GridItem(.flexible(minimum: 50)), GridItem(.flexible(minimum:50))]
    
	@StateObject var info = lastPrinterInfo.shared
	@StateObject var workImage = workingImage.shared
    @StateObject var clipboardHelp = clipboardHelper.shared
    @State var showProgress = false
    @State var stripeIterations : Int = 5
    @State var inchSize : Double = 4.0
    @State var colorMode : Int = grayscaleMode
    
    /// The public singleton of our panel
    static let shared: MainView = .init()
    
    init() {
        /* is anything needed? */
    }

    func onStart() {
        /* add view init stuff here */
    }
    
    func pinInchSize() {
        var inches = max( 1.0, inchSize )
        inches = min( 20.0, inches )
        inchSize = inches       // can't do validation on the fly in SwiftUI, just yet
    }
    
    func doStripes( dpi: Double ) {
        showProgress = true
        pinInchSize()
        
        var iter = max( 2, stripeIterations )
        iter = min( 8, iter )
        stripeIterations = iter     // can't do validation on the fly in SwiftUI, just yet
        
        workImage.createStripes( dpi: dpi, iterations: iter, inches: inchSize, mode: colorMode )
        
        showProgress = false
    }
    
    var body: some View {
        HStack {
        
            // image view
            VStack {
                Image(nsImage: workImage.image)
                    .antialiased( true )
                    .interpolation( Image.Interpolation.high )
                    .resizable()
                    .scaledToFit()
                    .frame(width: 72*imageFrameInches, height: 72*imageFrameInches ) // size is in points!
                    .imageScale(.large)
                
                if (workImage.isEmpty) {
                    Text("No Image")
                    .frame( minWidth: 72*imageFrameInches )
                } else {
                    HStack {
                        LabeledContent("Image Size H:", value: workImage.pixelSize.width, format: .number)
                        LabeledContent(" V:", value: workImage.pixelSize.height, format: .number)
                    }
                    .frame( minWidth: 72*imageFrameInches )
                    LabeledContent("ppi:", value: workImage.ppi, format: .number)
                }
                
            }   // end left VStack
            .padding(.all, 1.0)
            
            // divider
            Color.blue.frame(width: 2)
            
            VStack {
                Button("Empty Image") {
                    workImage.emptyImage()     // fast, doesn't need progress
                }
                Button("Solid White/Blank") {
                    pinInchSize()
                    workImage.whiteImage(inches: inchSize, mode: colorMode)
                }
                Button("Solid Black") {
                    pinInchSize()
                    workImage.blackImage(inches: inchSize, mode: colorMode)
                }
                
                Button("Stripes 200 dpi") {
                    doStripes( dpi: 100 )
                }
                Button("Stripes 300 dpi") {
                    doStripes( dpi: 150 )
                }
                Button("Stripes 360 dpi") {
                    doStripes( dpi: 180 )
                }
                
                LabeledContent("DPI Doublings:") {
                    TextField("Doublings", value: $stripeIterations, format: .number.precision(.significantDigits(1...2)))
#if false
// bah, compiler says .onChange requires MacOS 14.buggy
                        .onChange(of: stripeIterations) {
                            if (stripeIterations < 2) { stripeIterations = 2 }
                            if (stripeIterations > 10) { stripeIterations = 10 }
                        }
#endif
                        .fixedSize()
                }
                .fixedSize()
                
                LabeledContent("Size (inches):") {
                    TextField("Size", value: $inchSize, format: .number.precision(.fractionLength(1)))
#if false
// bah, compiler says .onChange requires MacOS 14.buggy
                        .onChange(of: inchSize) {
                            if (inchSize < 1.0) { inchSize = 1.0 }
                            if (inchSize > 20.0) { inchSize = 20.0 }
                        }
#endif
                        .fixedSize()
                }
                .fixedSize()
                
                Picker(selection: $colorMode, label: Text("Color Mode:")) {
                    Text("Grayscale").tag(grayscaleMode)
                    Text("RGB").tag(rgbMode)
                    Text("CMYK").tag(cmykMode)
                }
                .pickerStyle(RadioGroupPickerStyle())
                
                Color.blue.frame( width: 120, height: 2)
                .fixedSize()
                
                Spacer()    // push buttons to the top
                
#if false
                if (showProgress) { // TODO: we don't get redraw while the functions work, except for save
                    ProgressView()
                }
#endif
                
            }   // end middle VStack
            .padding(.all, 1.0)
            
            // divider
            Color.blue.frame(width: 2)
            
            // data view
            ScrollView(.vertical) {
                VStack {
                    LabeledContent("Printer Name:", value: info.name )
                    LabeledContent("Printer Model:", value: info.model)
                    
                    HStack {
                        LabeledContent("Printer Resolution H:", value: info.resH, format: .number)
                        LabeledContent(" V:", value: info.resV, format: .number)
                    }
                    .fixedSize()
                    
                    // divider
                    Color.blue.frame(height: 2)
                
                    LabeledContent("Resolution List Length:", value: info.resolutionsH.count, format: .number)
                    .fixedSize()

                    ForEach( 0..<info.resolutionsH.count, id: \.self) { index in
                        HStack {
                            Text("Resolution \(index+1)")
                            LabeledContent(" H:", value: info.resolutionsH[index], format: .number)
                            LabeledContent(" V:", value: info.resolutionsV[index], format: .number)
                        }
                    }   // end ForEach resolution
                    .fixedSize()
                    
                    // divider
                    Color.blue.frame(height: 2)
                    
                    Section {
                        Text( "Print Settings Dictionary:").frame(maxWidth: .infinity, alignment: .leading)
                            LazyVGrid(columns: columns, alignment: .leading) {
                                ForEach( info.settingsDict.sorted(by: >), id: \.key) { key, value in
                                    Text(key).frame(maxWidth: .infinity, alignment: .leading).border (.gray )
                                    Text(value).frame(maxWidth: .infinity, alignment: .leading).border (.gray )
                                }
                            } // end lazy v grid
                    }.collapsible(true)     // doesn't work everywhere yet, sigh.

                    // divider
                    Color.blue.frame(height: 2)
                    
                    Section {
                        // so far it never contains more than "isPrinter: YES",
                        Text( "Printer Description Dictionary:").frame(maxWidth: .infinity, alignment: .leading)
                        ScrollView {
                            LazyVGrid(columns: columns, alignment: .leading) {
                                ForEach( info.printDescDict.sorted(by: >), id: \.key) { key, value in
                                    Text(key).frame(maxWidth: .infinity, alignment: .leading).border (.gray )
                                    Text(value).frame(maxWidth: .infinity, alignment: .leading).border (.gray )
                                }
                            } // end lazy v grid
                        }
                    }.collapsible(true)     // doesn't work everywhere yet, sigh.
                    
                    Spacer()    // align above views to the top

                }   // end right VStack
            }   // end right scroll view
            .padding(.all, 1.0)
        
        }   // end main view Hstack
        .onReceive(NotificationCenter.default.publisher(for: NSWindow.didBecomeKeyNotification)) { notification in
            // we don't really need the value of the notification
            clipboardHelp.checkPasteboard()
            }
        .onAppear(perform: onStart)
        .padding()
    
    }   // end body of view

}   // end MainView

/* -------------------------------------------------------------------------------- */

#Preview {
    MainView()
}
