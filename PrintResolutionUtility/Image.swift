//
//  Image.swift
//  PrintResolutionUtility
//
//  Created by Chris Cox on December 6, 2023.
//

import AppKit

/* -------------------------------------------------------------------------------- */

let bitmapMode = 1
let grayscaleMode = 2
let rgbMode = 3
let cmykMode = 4

/* -------------------------------------------------------------------------------- */

public func createCGBitmapContext( mode: Int, bounds: CGRect ) -> CGContext {
    
    var bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.none.rawValue)
    if (mode == rgbMode) { bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.noneSkipLast.rawValue) }
    
    var bits = 8
    if (mode == bitmapMode) { bits = 1 }
    
    var space : CGColorSpace = CGColorSpaceCreateDeviceGray()
    // bitmap and grayscale both use the gray space
    if (mode == rgbMode) { space = CGColorSpaceCreateDeviceRGB() }
    if (mode == cmykMode) { space = CGColorSpaceCreateDeviceCMYK() }
    
    var rowBytes : Int = 0
    if (mode == bitmapMode) { rowBytes = (Int(bounds.size.width) + 7) / 8 }
    if (mode == rgbMode) { rowBytes = 4 * Int(bounds.size.width) }
    if (mode == cmykMode) { rowBytes = 4 * Int(bounds.size.width) }

    // looks like CGBitmapContext is crippled in Swift: doesn't support bitmap, only supports 32 bit XRGB
    let bitmapContext = CGContext( data: nil, width: Int(bounds.size.width), height: Int(bounds.size.height),
                                    bitsPerComponent: bits, bytesPerRow: rowBytes, space: space,
                                    bitmapInfo: bitmapInfo.rawValue)
    
// TODO: this could probably be made safer
    return bitmapContext!
}

/* -------------------------------------------------------------------------------- */

// NOTE: this would be faster if I wrote to the bitmap directly, but less readable for other developers
public func createStripeResolutionTest( dpi: Double, iterations: Int, inches: Double, mode: Int ) -> NSImage? {
    var output: NSImage? = nil
    let inchSize = inches       // 3 inches is pretty much minimum to get good sections for evaluation under microscope, but 4 is better
    let resolution = Double( 1 << (iterations-1) )     // dpi*resolution is final image dpi
    let dimension = floor(inchSize * dpi * resolution)
    let imageBounds = CGRectMake(0,0,dimension,dimension)
    let context = createCGBitmapContext( mode: mode, bounds: imageBounds )
    let section = Int(dimension / Double(iterations))
    
    // fill full image with solid white
    context.setFillColor( CGColor.white )
    context.fill( imageBounds )
    
    // draw stripe sections:   checkerboard, horizontal, vertical
    for block in 0...(iterations-1) {
        let top : Int = section * block
        let bottom : Int = section * (block+1)
        let third = Int( dimension / 3)
        let twothirds = Int( 2*third )
        let right: Int = Int( dimension )
        let height : Int = 1 << block
        let heightFloat = CGFloat( Double(height) )
        
        context.setBlendMode( CGBlendMode.copy )
        context.setFillColor( CGColor.black )
        
        // horizontal left to two thirds
        for y in stride(from: top, to: bottom, by: 2*height) {
            let thisRect = CGRectMake( 0, CGFloat(y), CGFloat(twothirds), heightFloat )
            context.fill( thisRect )
        }
        
        // vertical two thirds to right
        for x in stride(from: twothirds, to: right, by: 2*height) {
            let thisRect = CGRectMake( CGFloat(x), CGFloat(top), heightFloat, CGFloat(section) )
            context.fill( thisRect )
        }
        
        // vertical left to third to create checkerboard
        context.setBlendMode( CGBlendMode.difference )
        context.setFillColor( CGColor.white )
        for x in stride(from: 0, to: third, by: 2*height) {
            let thisRect = CGRectMake( CGFloat(x), CGFloat(top), heightFloat, CGFloat(section) )
            context.fill( thisRect )
        }
        
        context.setBlendMode( CGBlendMode.copy )
        context.setFillColor( CGColor.black )

        // draw text labels, with a white border to make them more visible
        context.saveGState()
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        let fontName = "Arial-Bold" as CFString
        let fontSize = 16.0 * (dpi * resolution / 72.0)
        let font = CTFontCreateWithName(fontName, fontSize, nil)
        let attrs: [NSAttributedString.Key : Any] = [.font: font, .foregroundColor: CGColor.black,
                        .strokeColor: CGColor.white, .strokeWidth: -3.0, .paragraphStyle: paragraphStyle ]

        let ppicalc = Int(dpi * resolution) >> block
        let ppitext = String( ppicalc )
        let textString = ppitext + "ppi"
        let attributedString = NSAttributedString(string: textString, attributes: attrs)
        let line = CTLineCreateWithAttributedString(attributedString)
        
        // the CT effects on the context are beyond messed up:  there appears to be a completely different context for text inside the CGContext
        context.textMatrix = .identity
        context.translateBy(x: fontSize/4, y: CGFloat(Double(bottom - section) + fontSize/4))
        CTLineDraw( line, context )
        context.flush()
        context.restoreGState()     // why does this not restore the text state?
        
// TODO: show progress while drawing!

    }   // end for block in iterations
    
    if let image = context.makeImage() {
        output = NSImage(cgImage: image, size: imageBounds.size)
    }

    return output
}

/* -------------------------------------------------------------------------------- */

public func creatSolidImage( color: CGColor, dpi: Double, inches: Double, mode: Int ) -> NSImage? {
    var output: NSImage? = nil
    let inchSize = inches
    let dimension = floor(inchSize * dpi)
    let imageBounds = CGRectMake(0,0,dimension,dimension)
    let context = createCGBitmapContext( mode: mode, bounds: imageBounds )

    context.setFillColor( color )
    context.fill( imageBounds )
    if let image = context.makeImage() {
        output = NSImage(cgImage: image, size: imageBounds.size)
    }
    
    return output
}

/* -------------------------------------------------------------------------------- */

final class workingImage : ObservableObject {

    /// The public singleton of our image data
    static let shared = workingImage()
    
    @Published var image : NSImage
    @Published var isEmpty = true
    @Published var ppi : Double = 0
    @Published var pixelSize : NSSize = NSSize( width: 0, height: 0 )
    
    init() {
        // Swift complains that we can't call emptyImage yet! Lame!
        image = NSImage.init( size: NSSize( width: 72, height: 72 ) )
        isEmpty = true
        ppi = 72
    }
    
    /// create empty image (just to be safe)
    func emptyImage() {
        image = NSImage.init( size: NSSize( width: 72, height: 72 ) )
        isEmpty = true
        ppi = 72
    }
    
    /// create solid white image
    func whiteImage(inches: Double, mode: Int) {
        let new_ppi = 72.0
        let temp = creatSolidImage( color: CGColor.white, dpi: new_ppi, inches: inches, mode: mode )
        if (temp != nil) {
            image = temp!
            isEmpty = false
            pixelSize = image.size
            ppi = new_ppi
        }
    }
    
    /// create solid black image
    func blackImage(inches: Double, mode: Int) {
        let new_ppi = 72.0
        let temp = creatSolidImage( color: CGColor.black, dpi: new_ppi, inches: inches, mode: mode)
        if (temp != nil) {
            image = temp!
            isEmpty = false
            pixelSize = image.size
            ppi = new_ppi
        }
    }
    
    func updateResolutionFromImage() {
        let ScreenSize = image.size
        var width : Int = 0
        var height : Int = 0
        // Find the largest image rep, since thumbnails are sometimes included for some reason..
        // Sometimes gets 2X the actual image size, Whiskey Tango Foxtrot?
        for imageRep in image.representations {
            if (imageRep.pixelsWide > width) { width = imageRep.pixelsWide }
            if (imageRep.pixelsHigh > height) { height = imageRep.pixelsHigh }
        }
        pixelSize = NSSize( width: width, height: height )
        
        // width and height calcs should be the same, but may need to debug
        let resolution = 72.0*Double(width)/Double(ScreenSize.width)
        //let resolution2 = 72.0*Double(height)/Double(ScreenSize.height)
        ppi = resolution
    }
    
    func updateFromPaste( image: NSImage ) {
        self.image = image
        isEmpty = false
        updateResolutionFromImage()
    }

    /// create resolution test image
    func createStripes( dpi: Double, iterations: Int, inches: Double, mode: Int ) {
        let temp = createStripeResolutionTest( dpi: dpi, iterations: iterations, inches: inches, mode: mode )
        if (temp != nil) {
            image = temp!
            isEmpty = false
            pixelSize = image.size
            // we rendered the image without resolution, so we'll change it to what we know
            let resolution = Double( 1 << (iterations-1) )
            ppi = dpi * resolution
        }
    }
    
    /// read image from file
    func chooseFile() {
        var filename : URL?
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = false
        panel.canChooseDirectories = false
        panel.allowedContentTypes = [ .image ]

        if ( panel.runModal() == NSApplication.ModalResponse.OK ) {
            filename = panel.url
        }

        if (filename != nil) {
            let tempImage = NSImage( contentsOf: filename! )
            if (tempImage != nil) {
                image = tempImage!
                isEmpty = false
                updateResolutionFromImage()
            }
        }
    }   // end chooseFile
    
    /// save image to TIFF file, without dpi data :-(
    func saveFile() {
        // we can't easily get the image mode, so we'll use TIFF because it can support all color modes
        let panel = NSSavePanel()
        panel.allowedContentTypes = [ .tiff ]
        
        var filename : URL?
        if ( panel.runModal() == NSApplication.ModalResponse.OK ) {
            filename = panel.url
        }

        // But Apple's code cannot save resolution data, and PNG isn't any better (and doesn't support CMYK)
        if (filename != nil) {
            let imageRep = NSBitmapImageRep(data: image.tiffRepresentation!)
            // why the heck doesn't Apple support ZIP/FLATE in TIFF? Are they stuck in the 1990s?
            imageRep?.setCompression( NSBitmapImageRep.TIFFCompression.lzw, factor: 0.5 )
            let tiffData = imageRep?.representation( using: NSBitmapImageRep.FileType.tiff, properties: [:])
            
            do {
                try tiffData?.write( to: filename! )
            } catch {
                print("file save failed")   // TODO: need better error handling
            }

// TODO: convert to bitmap, write my own TIFF code, save resolution value in file
// Why the heck doesn't the OS code handle image resolution?
        
        }   // end filename != nil

    }   // end saveFile

}
